package com.empresaTest.programa.model;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class CustomException extends RuntimeException{

    private Integer code;
    private String title;
    private String detail;
    private HttpStatus httpStatus;

    public CustomException(Integer code, String title, String detail, HttpStatus httpStatus) {
        super(title);
        this.code = code;
        this.title = title;
        this.detail = detail;
        this.httpStatus = httpStatus;
    }
}
