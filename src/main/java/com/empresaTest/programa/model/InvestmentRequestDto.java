package com.empresaTest.programa.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InvestmentRequestDto {

    private Long investorId;
    private Long projectId;
    private BigDecimal amount;

}
