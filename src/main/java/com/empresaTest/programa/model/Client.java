package com.empresaTest.programa.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "cliente")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String firstName;
    private String middleName;
    private String lastName;
    private String middleLastName;
    @Column(nullable = false, length = 12)
    private String phone;
    private String address;
    private String cityResidence;

}
