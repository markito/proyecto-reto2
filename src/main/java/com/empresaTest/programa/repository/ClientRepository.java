package com.empresaTest.programa.repository;

import com.empresaTest.programa.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository  extends JpaRepository<Client, Long> {


}
