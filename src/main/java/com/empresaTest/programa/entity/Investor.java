package com.empresaTest.programa.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Table(name = "investor")
public class Investor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String LastName;
    private String phone;
    private String address;
    private String cityResidence;
    private String document;
    private TypeInvestor typeInvestor;
    private BigDecimal monthlySalary;

}
