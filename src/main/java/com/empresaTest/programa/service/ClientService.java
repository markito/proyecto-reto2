package com.empresaTest.programa.service;

import com.empresaTest.programa.model.Client;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface ClientService {
    ResponseEntity<Client> getData(String documentType, String documentNumber);

    Client saveClient(Client client);

    Optional<Client> getClientId(Long id);

    List<Client> getAllClient();

    void deleteClient(Long id);

    Client updateClient(Long id, Client client);
}
