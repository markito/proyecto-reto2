package com.empresaTest.programa.service;

import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.model.Client;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface InvestorService {

    // obtener un inversionista por documento
    Optional<Investor> getInvestor(String document);
    // obtener un inversionista por id
    Optional<Investor> getInvestorId(Long id);

    // obtener todos los inversionistas
    List<Investor> getAllInvestor();

    // guardar nuevo inversionista
    Investor saveInvestor(Investor investor);

    // Editar inversionista
    Investor updateInvestor(Long id, Investor investor);

    // Eliminar inversionista
    void deleteInvestor(Long id);

}
