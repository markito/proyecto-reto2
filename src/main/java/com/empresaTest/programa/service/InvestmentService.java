package com.empresaTest.programa.service;

import com.empresaTest.programa.entity.Investment;
import com.empresaTest.programa.model.InvestmentRequestDto;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InvestmentService {

    Investment saveInvestment(InvestmentRequestDto investment);

    List<Investment> getAllInvestmentByProjectName(String proyectName);

    List<Investment> getAllInvestmentByProjectId(Long proyectid);
}
